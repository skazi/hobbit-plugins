#!/usr/bin/perl -w

# Copyright (C) 2007, 2008 Christoph Berg <myon@debian.org>
# Copyright (C) 2010 Axel Beckert <abe@debian.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

use strict;
use Hobbit;

$ENV{'PATH'} = '/bin:/sbin:/usr/bin:/usr/sbin';
$ENV{'LC_ALL'} = 'C';
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

exit 0 unless -x '/usr/sbin/postfix';

my $bb = new Hobbit('sftbnc');

my $maincf = '/etc/postfix/main.cf';
if (open(MAIN, '<', $maincf)) {
    my $softbounce = 0;
    while ($_ = <MAIN>) {
	next if /^\#/;
	$softbounce = 1 if /^\s*soft_bounce\s*=\s*yes\b/i;
    }
    close(MAIN);

    $bb->color_line($softbounce ? 'yellow' : 'green',
		    "Postfix soft_bounce is ".($softbounce ? 'on' : 'off'));
} else {
    $bb->color_line('yellow', "Postfix seems to be installed but I can't open $maincf: $!");
}
$bb->send;
