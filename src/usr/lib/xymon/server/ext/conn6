#!/usr/bin/perl
#
# Do IPv6 ping tests as "conn6".
# This server script is based on a shell script from Debian hobbit-plugins
# 20101207 package, but rewritten in Perl.
#
# Just add conn6 to the services of a host and this script will ping6 the
# AAAA record of the given host.
# Alternatively you can add a comma separated list of IPv6 addresses or
# hostnames that resolve to IPv6 addresses as a parameter to conn6, to ping6
# all these addresses.
# Examples:
# 10.1.2.3 foo # ftp conn6:2001:db8:1::42
# 10.1.2.4 bar # smtp conn6
# 10.1.2.5 baz # conn6:baz.example.com dialup
# 10.1.2.6 router # conn6:2001:db8:1::1,2001:db8:2::1
#
# Copyright (c) 2011  Roland Rosenfeld <roland@spinnaker.de>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# $Id: conn6.pl,v 1.2 2011/02/22 09:22:55 roland Exp roland $

use strict;
use warnings;
#use Data::Dumper;

my $fping = 'fping6';

open(HOSTGREP, '-|', "$ENV{XYMONHOME}/bin/xymongrep conn6 conn6:\* dialup")
  || die "cannot run $ENV{XYMONHOME}/bin/xymongrep";
while (<HOSTGREP>) {
   if (/^(\d+\.\d+\.\d+\.\d+)\s+(\S+)\s+#\s+(.*)$/) {
      my ($ipv4, $host, $services) = ($1, $2, $3);

      # parse services:
      my $dialup = 0;
      my @destinations;
      foreach my $service (split /\s+/, $services) {
         if ($service eq 'dialup') {
            $dialup = 1;
         } elsif ($service eq 'conn6') {
            push @destinations, $host;
         } elsif ($service =~ /^conn6:(.*)$/) {
            my $dests = $1;
            foreach my $destination (split /,/, $dests) {
               push @destinations, $destination;
            }
         }
      }
      next if $#destinations == -1; # no conn6 found, only dialup

      my $output = '';
      my $color = 'green';
      foreach my $destination (@destinations) {
         my $localcolor = 'green';
         my $pingresult = `$fping -e $destination 2>&1`;
         my $time = 0;
         if ($pingresult =~ /is\s*alive/) {
            if ($pingresult =~ /\(([\d.]+)\s*us/) {
               $time = sprintf('%f', $1/1000000);
            } elsif ($pingresult =~ /\(([\d.]+)\s*ms/) {
               $time = sprintf('%f', $1/1000);
            } elsif ($pingresult =~ /\(([\d.]+)/) {
               $time = $1;
            }
         } else {
            $color = 'red';
            $localcolor = 'red';
            if ($dialup == 1) {
               $color = 'clear';
               $localcolor = 'clear';
            }
         }
         $output .= "\&$localcolor $pingresult";
         if ($destination !~ /:.*:/) {
            # resolve hostnames:
            $output .= `host -t AAAA $host 2>&1`;
         }
         $output .= "Seconds: $time\n\n\n";
      }

      #open (BB, '|-', "cat") || die "cannot run bb";
      open(BB, '|-', "$ENV{XYMON} $ENV{XYMSRV} \@") || die "cannot run $ENV{XYMON}";
      print BB "status $host.conn6 $color ".`date`."\n";
      print BB $output;
      #print BB "------------------------------------------\n";
      close BB;
   }
}

