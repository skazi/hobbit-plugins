#!/usr/bin/perl -w

# Script to generate a zombie process to test misc.d/zombies.
# Author: Axel Beckert <abe@debian.org>

use 5.010;

my $pid = fork;

if ($pid) {
    say "PARENT: Parent = $$. Child = $pid";
    # Parent stops itself
    kill(19, $$);
    say "PARENT: continued...";
    exit 0;
} elsif (defined $pid and $pid == 0) {
    say "CHILD: Child = $$";
    sleep 1;
    say "CHILD: Back from sleep, exiting"
} else {
    say "FORK failed: $!";
}
